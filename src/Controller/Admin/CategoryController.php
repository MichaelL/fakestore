<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use App\Form\CategoryType;
use App\Repository\CategoriesRepository;
use App\Service\ErrorsMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/admin/category", name="admin.category.")
 */
class CategoryController extends AbstractController
{
    /**
     * @var CategoriesRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SluggerInterface
     */
    private $slugger;
    /**
     * @var ErrorsMessage
     */
    private $errorsMessage;

    public function __construct(CategoriesRepository $repository, EntityManagerInterface $em, ErrorsMessage $errorsMessage, SluggerInterface $slugger)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->slugger = $slugger;
        $this->errorsMessage = $errorsMessage;
    }

    /**
     * @Route("", name="index")
     * @return Response
     */
    public function index(): Response
    {
        $categories = $this->repository->findAll();

        return $this->render('admin/category/index.html.twig',
            compact('categories'));
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $category = new Categories();
        $form = $this->createForm(CategoryType::class, $category);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setSlug(strtolower($this->slugger->slug($form->get('title')->getData())));
            $this->em->persist($category);
            $this->em->flush();
            $this->addFlash('success', 'Catégorie créée.');
            return $this->redirectToRoute('admin.category.index');
        }
        $errors = $this->errorsMessage->errorsMessage($form);

        return $this->render('admin/category/create.html.twig',
            compact('category', 'errors', 'form_view'));
    }

    /**
     * @Route("/{id}", name="edit")
     * @param Categories $category
     * @param Request $request
     * @return Response
     */
    public function edit(Categories $category, Request $request): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setSlug(strtolower($this->slugger->slug($form->get('title')->getData())));
            $this->em->flush();
            $this->addFlash('success', 'Catégorie modifiée.');
            return $this->redirectToRoute('admin.category.index');
        }
        $errors = $this->errorsMessage->errorsMessage($form);

        return $this->render('admin/category/edit.html.twig',
            compact('category', 'errors', 'form_view'));
    }

    /**
     * @Route("/delete/{id}/{token}", name="delete")
     * @param Categories $category
     * @param CsrfTokenManagerInterface $csrf
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Categories $category, CsrfTokenManagerInterface $csrf, Request $request): RedirectResponse
    {
        $token = new CsrfToken('delete_category', $request->attributes->get('token'));

        if (!$csrf->isTokenValid($token)) {
            $this->addFlash('danger', 'Jeton CSRF invalide.');
        }
        $this->em->remove($category);
        $this->em->flush();
        $this->addFlash('success', 'Catégorie supprimée.');
        return $this->redirectToRoute('admin.category.index');
    }
}