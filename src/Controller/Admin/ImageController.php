<?php

namespace App\Controller\Admin;

use App\Entity\Images;
use App\Entity\Products;
use App\Repository\ImagesRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * @Route("/admin/image", name="admin.image.")
 */
class ImageController extends AbstractController
{
    /**
     * @var ImagesRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var FileUploader
     */
    private $uploader;

    public function __construct(ImagesRepository $repository, EntityManagerInterface $em, FileUploader $uploader)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->uploader = $uploader;
    }

    /**
     * @Route("/active/{id}", name="active")
     * @param Images $image
     * @return JsonResponse
     */
    public function imageActive(Images $image): JsonResponse
    {
        $product = $image->getProduct();
        $active = $this->repository->findActive($product);
        if (!empty($active)) {
            $active[0]->setActive(false);
        }
        $image->setActive(true);
        $this->em->flush();

        return new JsonResponse($image->getActive());
    }

    /**
     * @Route("/delete/{product}/{id}/{token}", name="delete")
     * @param Products $product
     * @param Images $image
     * @param CsrfTokenManagerInterface $csrf
     * @param Request $request
     * @return JsonResponse
     */
    public function imageDelete(Products $product, Images $image, CsrfTokenManagerInterface $csrf, Request $request): JsonResponse
    {
        if (count($this->repository->findBy(['product' => $product])) !== 1) {
            if ($image->getActive()) {
                return new JsonResponse('active');
            }
            $token = new CsrfToken('delete_image', $request->attributes->get('token'));

            if (!$csrf->isTokenValid($token)) {
                return new JsonResponse('csrf');
            }
            $filename = $image->getName();
            $dir = $product->getId();
            $this->uploader->delete($dir, $filename);

            $this->em->remove($image);
            $this->em->flush();
            return new JsonResponse(true);
        }

        return new JsonResponse(false);
    }
}