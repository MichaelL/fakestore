<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Form\AdminLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/fs-", name="fs.")
 */
class LoginController extends AbstractController
{
    /**
     * @Route("login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('admin.index');
        }

        $admin = new Admin();
        $form = $this->createForm(AdminLoginType::class, $admin);
        $form_view = $form->createView();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && $this->getUser()) {
            return $this->redirectToRoute('admin.index');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('admin/security/login.html.twig', compact('form_view','error'));
    }

    /**
     * @Route("logout", name="logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
