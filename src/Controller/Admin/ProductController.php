<?php

namespace App\Controller\Admin;

use App\Entity\Products;
use App\Form\ProductType;
use App\Repository\ProductsRepository;
use App\Service\ErrorsMessage;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/admin/product", name="admin.product.")
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductsRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ErrorsMessage
     */
    private $errorsMessage;
    /**
     * @var FileUploader
     */
    private $uploader;
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(ProductsRepository $repository, EntityManagerInterface $em, ErrorsMessage $errorsMessage, FileUploader $uploader, SluggerInterface $slugger)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->errorsMessage = $errorsMessage;
        $this->uploader = $uploader;
        $this->slugger = $slugger;
    }

    /**
     * @Route("", name="index")
     * @return Response
     */
    public function index(): Response
    {
        $products = $this->repository->findAllDesc();

        return $this->render('admin/product/index.html.twig',
            compact('products'));
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $product = new Products();
        $form = $this->createForm(ProductType::class, $product);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $product->setSlug(strtolower($this->slugger->slug($form->get('title')->getData())));
            $this->em->persist($product);
            $this->em->flush();

            $imageUpload = $form->get('images')->getData();
            $this->uploader->productImage($imageUpload, $product);

            $this->addFlash('success', 'Produit créé.');
            return $this->redirectToRoute('admin.product.index');
        }
        $errors = $this->errorsMessage->errorsMessage($form);

        return $this->render('admin/product/create.html.twig',
            compact('product', 'errors', 'form_view'));
    }

    /**
     * @Route("/{id}", name="edit")
     * @param Products $product
     * @param Request $request
     * @return Response
     */
    public function edit(Products $product, Request $request): Response
    {
        $form = $this->createForm(ProductType::class, $product, ['required' => false]);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product->setSlug(strtolower($this->slugger->slug($form->get('title')->getData())));
            $this->em->flush();

            $imageUpload = $form->get('images')->getData();
            $this->uploader->productImage($imageUpload, $product);

            $this->addFlash('success', 'Produit édité.');
            return $this->redirectToRoute('admin.product.index');
        }
        $errors = $this->errorsMessage->errorsMessage($form);

        return $this->render('admin/product/edit.html.twig',
            compact('product', 'errors', 'form_view'));
    }

    /**
     * @Route("/delete/{id}/{token}", name="delete")
     * @param Products $product
     * @param CsrfTokenManagerInterface $csrf
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Products $product, CsrfTokenManagerInterface $csrf, Request $request): RedirectResponse
    {
        $token = new CsrfToken('delete_product', $request->attributes->get('token'));

        if (!$csrf->isTokenValid($token)) {
            $this->addFlash('danger', 'Jeton CSRF invalide.');
        }
        $images = $product->getImages();
        $dir = $product->getId();
        foreach ($images as $image) {
            $filename = $image->getName();
            $this->uploader->delete($dir, $filename);

            $this->em->remove($image);
        }
        $this->em->flush();
        $this->uploader->delete($dir);

        $this->em->remove($product);
        $this->em->flush();
        $this->addFlash('success', 'Produit supprimé.');
        return $this->redirectToRoute('admin.product.index');
    }
}