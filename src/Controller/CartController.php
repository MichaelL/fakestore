<?php

namespace App\Controller;

use App\Entity\Addresses;
use App\Entity\Products;
use App\Form\AddressType;
use App\Service\Calculator;
use App\Service\Cart;
use App\Service\ErrorsMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panier", name="panier.")
 */
class CartController extends AbstractController
{
    /**
     * @var RequestStack
     */
    private $session;
    /**
     * @var Calculator
     */
    private $calculator;
    /**
     * @var Cart
     */
    private $cart;
    /**
     * @var ErrorsMessage
     */
    private $errorsMessage;

    public function __construct(RequestStack $requestStack, Calculator $calculator, Cart $cart, ErrorsMessage $errorsMessage)
    {
        $this->session = $requestStack->getSession();
        $this->calculator = $calculator;
        $this->cart = $cart;
        $this->errorsMessage = $errorsMessage;
    }

    /**
     * @Route("", name="index")
     * @return Response
     */
    public function index(): Response
    {
        $current_page = "cart";
        $products = $this->cart->get();
        $total = $this->calculator->total($products);

        $address = $this->getUser()->getAddress();
        if (!$address) {
            $address = new Addresses();
        }
        $form = $this->createForm(AddressType::class, $address);
        $form_view = $form->createView();
        $errors = $this->errorsMessage->errorsMessage($form);

        return $this->render('pages/cart.html.twig', compact('current_page', 'products', 'total', 'form_view', 'errors'));
    }

    /**
     * @Route("/ajout/{id}", name="ajout")
     * @param Products $product
     * @return JsonResponse
     */
    public function add(Products $product): JsonResponse
    {
        return $this->cart->add($product);
    }

    /**
     * @Route("/retirer/{id}", name="retirer")
     * @param Products $product
     * @return JsonResponse
     */
    public function pull(Products $product): JsonResponse
    {
        return $this->cart->pull($product);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     * @param Products $product
     * @return JsonResponse
     */
    public function remove(Products $product): JsonResponse
    {
        return $this->cart->remove($product);
    }

    /**
     * @Route("/vider", name="vider")
     */
    public function removeAll(): RedirectResponse
    {
        $this->session->remove("panier");
        return $this->redirectToRoute('panier.index');
    }
}