<?php

namespace App\Controller\Customer;

use App\Entity\Addresses;
use App\Form\AddressType;
use App\Repository\AddressesRepository;
use App\Service\ErrorsMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class AddressController extends AbstractController
{
    /**
     * @Route("/address", name="adresse")
     * @param AddressesRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ErrorsMessage $errorsMessage
     * @return Response
     */
    public function index(AddressesRepository $repository, Request $request, EntityManagerInterface $em, ErrorsMessage $errorsMessage): Response
    {
        $current_page = "address";

        $address = $repository->findOneBy(['customer' => $this->getUser()]);
        if (!$address) {
            $address = new Addresses();
        }
        $form = $this->createForm(AddressType::class, $address);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address->setCustomer($this->getUser());
            $em->persist($address);
            $em->flush();
        }
        $errors = $errorsMessage->errorsMessage($form);

        return $this->render('customer/address.html.twig',
            compact('address', 'errors', 'form_view', 'current_page'));
    }
}