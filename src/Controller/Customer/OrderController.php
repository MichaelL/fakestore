<?php

namespace App\Controller\Customer;

use App\Entity\Ordered;
use App\Service\Cart;
use App\Service\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class OrderController extends AbstractController
{
    /**
     * @var Cart
     */
    private $cart;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var Order
     */
    private $order;

    public function __construct(Order $order, Cart $cart, EntityManagerInterface $em, RequestStack $requestStack)
    {
        $this->cart = $cart;
        $this->em = $em;
        $this->session = $requestStack->getSession();
        $this->order = $order;
    }

    /**
     * @Route("/order", name="commande")
     * @return Response
     */
    public function index(): Response
    {
        $current_page = "order";
        $orders = $this->order->getUserOrders($this->getUser());

        return $this->render('customer/order.html.twig', compact('orders', 'current_page'));
    }

    /**
     * @Route("/ordered", name="commander")
     * @return JsonResponse
     */
    public function order(): JsonResponse
    {
        $order = new Ordered();
        $products = $this->cart->getCart();
        $order->setCustomer($this->getUser());
        $order->setProducts($products);
        $order->setPrice($_POST['totalPrice']);
        $this->em->persist($order);
        $this->em->flush();
        $this->session->remove("panier");

        return new JsonResponse(true);
    }
}