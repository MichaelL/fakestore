<?php

namespace App\Controller\Customer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class PaymentController extends AbstractController
{
    /**
     * @Route("/payment", name="paiement")
     * @return Response
     */
    public function index(): Response
    {
        $current_page = "payment";

        return $this->render('customer/payment.html.twig', compact('current_page'));
    }
}