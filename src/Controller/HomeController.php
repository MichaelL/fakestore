<?php

namespace App\Controller;

use App\Repository\ProductsRepository;
use App\Service\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     * @param ProductsRepository $productsR
     * @param Calculator $calculator
     * @return Response
     */
    public function index(ProductsRepository $productsR, Calculator $calculator): Response
    {
        $products = $productsR->findLatest();
        foreach ($products as $product) {
            $product->discounted = $calculator->promotion($product->getPrice(), $product->getPromotion());
        }
        return $this->render('pages/home.html.twig',
            compact('products'));
    }
}