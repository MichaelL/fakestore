<?php

namespace App\Controller;

use App\Repository\CategoriesRepository;
use App\Repository\ProductsRepository;
use App\Service\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @var ProductsRepository
     */
    private $productsR;
    /**
     * @var CategoriesRepository
     */
    private $categoriesR;
    /**
     * @var Calculator
     */
    private $calculator;

    public function __construct(ProductsRepository $productsR, CategoriesRepository $categoriesR, Calculator $calculator)
    {
        $this->productsR = $productsR;
        $this->categoriesR = $categoriesR;
        $this->calculator = $calculator;
    }

    /**
     * @Route("/produits/{slug}", name="produits")
     * @param string|null $slug
     * @return Response
     */
    public function index(string $slug = null): Response
    {
        $current_page = "products";
        if ($slug !== null) {
            $category = $this->categoriesR->findOneBy(['slug' => $slug]);
            $products = $this->productsR->findByCategory($category);
        } else {
            $products = $this->productsR->findAllDesc();
        }
        foreach ($products as $product) {
            $product->discounted = $this->calculator->promotion($product->getPrice(), $product->getPromotion());
        }

        $categories = $this->categoriesR->findAllAsc();
        return $this->render('product/index.html.twig', compact('current_page', 'products', 'categories', 'slug'));
    }

    /**
     * @Route("/produit/{slug}", name="produit")
     * @param string $slug
     * @return Response
     */
    public function product(string $slug): Response
    {
        $product = $this->productsR->findOneBy(['slug' => $slug]);
        $product->discounted = $this->calculator->promotion($product->getPrice(), $product->getPromotion());
        return $this->render('product/product.html.twig', compact('product'));
    }
}