<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Form\RegistrationType;
use App\Service\ErrorsMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHash, ErrorsMessage $errorsMessage): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('accueil');
        }

        $user = new Customer();
        $form = $this->createForm(RegistrationType::class, $user);
        $form_view = $form->createView();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // hash le password
            $user->setPassword(
                $userPasswordHash->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $user->setRoles(["ROLE_USER"]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Vous êtes désormais inscrit. Identifiez-vous :');
            return $this->redirectToRoute('login');
        }
        $errors = $errorsMessage->errorsMessage($form);
        $errors["match"] = $errors["password"]->getForm()["first"]->getErrors();

        return $this->render('security/register.html.twig', compact('errors', 'form_view'));
    }
}
