<?php

namespace App\Form;

use App\Entity\Products;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('price', null,
            [
                'attr' => [
                    'step' => '.01'
                ]
            ])
            ->add('description')
            ->add('category', null,
            [
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('promotion', null, [
                'data' => 0
            ])
            ->add('images', FileType::class, [
                'mapped' => false,
                'attr' => [
                    'required' => $options['required']
                ],
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/jpeg'
                        ]
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
            'translation_domain' => 'forms'
        ]);
    }
}
