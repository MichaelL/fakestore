<?php

namespace App\Repository;

use App\Entity\Categories;
use App\Entity\Products;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Products|null find($id, $lockMode = null, $lockVersion = null)
 * @method Products|null findOneBy(array $criteria, array $orderBy = null)
 * @method Products[]    findAll()
 * @method Products[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Products::class);
    }

    public function findAllDesc()
    {
        return $this->findDescQuery()
            ->getQuery()
            ->getResult();
    }

    public function findLatest(): array
    {
        return $this->findDescQuery()
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }

    public function findByCategory(Categories $category)
    {
        return $this->findDescQuery()
            ->innerJoin("p.category", "c")
            ->andWhere("c.id = :category")
            ->setParameter("category", $category)
            ->getQuery()
            ->getResult();
    }

    private function findDescQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.create_at', 'DESC');
    }

    // /**
    //  * @return Products[] Returns an array of Products objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Products
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
