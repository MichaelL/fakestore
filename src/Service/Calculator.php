<?php

namespace App\Service;

class Calculator
{
    public function promotion($price, $promo): string
    {
        return number_format($price-$promo/100*$price, 2);
    }

    public function total($products): string
    {
        $total = 0;
        foreach ($products as $product) {
            if ($product->getPromotion() !== 0) {
                $product->setPrice($this->promotion($product->getPrice(), $product->getPromotion()));
            }
            $total += $product->getPrice() * $product->quantity;
        }
        return number_format($total, 2);
    }
}