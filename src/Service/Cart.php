<?php

namespace App\Service;

use App\Repository\ProductsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class Cart
{
    /**
     * @var RequestStack
     */
    private $session;
    /**
     * @var ProductsRepository
     */
    private $productsR;

    public function __construct(RequestStack $requestStack, ProductsRepository $productsR)
    {
        $this->session = $requestStack->getSession();
        $this->productsR = $productsR;
    }

    public function get(): array
    {
        $panier = $this->session->get("panier", []);
        $products = [];

        foreach ($panier as $id => $quantity) {
            $product = $this->productsR->find($id);
            $product->quantity = $quantity;
            $products[] = $product;
        }

        return $products;
    }

    public function getCart()
    {
        return $this->session->get("panier", []);
    }

    public function add($product): JsonResponse
    {
        $panier = $this->session->get("panier", []);
        $id = $product->getId();

        if (empty($panier[$id])) {
            $panier[$id] = 1;
        } else {
            $panier[$id]++;
        }

        $this->session->set("panier", $panier);
        return new JsonResponse($panier);
    }

    public function pull($product): JsonResponse
    {
        $panier = $this->session->get("panier", []);
        $id = $product->getId();

        if ($panier[$id] > 1) {
            $panier[$id] -= 1;
        }

        $this->session->set("panier", $panier);
        return new JsonResponse($panier);
    }

    public function remove($product): JsonResponse
    {
        $panier = $this->session->get("panier", []);
        $id = $product->getId();

        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }

        $this->session->set("panier", $panier);
        return new JsonResponse($panier);
    }
}