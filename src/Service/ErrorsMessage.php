<?php

namespace App\Service;

class ErrorsMessage
{
    public function errorsMessage($form): array
    {
        $errors = [];
        foreach ($form as $fieldName => $formField) {
            // each field has an array of errors
            $errors[$fieldName] = $formField->getErrors();
        }
        return $errors;
 }
}