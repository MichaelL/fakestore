<?php

namespace App\Service;

use App\Entity\Images;
use App\Repository\ImagesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $directory;
    private $slugger;
    private $em;
    /**
     * @var ImagesRepository
     */
    private $imagesR;

    public function __construct($directory, SluggerInterface $slugger, EntityManagerInterface $em, ImagesRepository $imagesR)
    {
        $this->directory = $directory;
        $this->slugger = $slugger;
        $this->em = $em;
        $this->imagesR = $imagesR;
    }

    public function upload(UploadedFile $file, $idDirectory): string
    {
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($filename).'.'.$file->guessExtension();

        try {
            $file->move($this->getDirectory().'/'.$idDirectory, $safeFilename);
        } catch (FileException $e) {

        }

        return $safeFilename;
    }

    public function delete($idDirectory, $file = '')
    {
        $fileSys = new Filesystem();
        $fileSys->remove($this->getDirectory().'/'.$idDirectory.'/'.$file);
    }

    /**
     * @param $imageUpload
     * @param $product
     */
    public function productImage($imageUpload, $product)
    {
        if ($imageUpload) {
            $image = new Images();
            $filename = $this->upload($imageUpload, $product->getId());
            $image->setName($filename);
            $image->setProduct($product);
            if (empty($this->imagesR->findActive($product))) {
                $image->setActive(true);
            }
            $this->em->persist($image);
            $this->em->flush();
        }
    }
    
    /**
     * @return mixed
     */
    public function getDirectory()
    {
        return $this->directory;
    }
}