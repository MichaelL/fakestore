<?php

namespace App\Service;

use App\Repository\OrderedRepository;
use App\Repository\ProductsRepository;

class Order
{
    /**
     * @var OrderedRepository
     */
    private $repository;
    /**
     * @var ProductsRepository
     */
    private $productsR;

    public function __construct(OrderedRepository $repository, ProductsRepository $productsR)
    {
        $this->repository = $repository;
        $this->productsR = $productsR;
    }

    public function getUserOrders($user)
    {
        $orders = $this->repository->findOrder($user);
        $products = [];

        foreach ($orders as $k => $order) {
            $products[$k]["price"] = $order["price"];
            foreach ($order["products"] as $product => $quantity) {
                $product = $this->productsR->find($product);
                $product->quantity = $quantity;
                $products[$k][] = $product;
            }
        }

        return $products;
    }
}